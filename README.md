html5-canvas-drawing-app
========================

Sketchpad app using html5 canvas to draw using touch or mouse, works on iOS, Android, Window Phone and browser.
App uses touch events, MSPointer events and mouse events to support iOS, Android, Window Phone and desktop browser.


![alt tag](https://raw.github.com/krisrak/html5-canvas-drawing-app/master/screenshot.png)

forked from https://github.com/krisrak/html5-canvas-drawing-app